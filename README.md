 实现魔方游戏效果。即由很多图片拼成的格子中，每行每列都可以左右循环移动。

    开发者说：这是很久之看过的一个游戏的操作，觉得很好，就试着写写了。此Demo只用到了UIButton(为了方便以后做点击事情的处理)。逻辑简单，没有用到复杂的算法和数据结构，简单易懂。

    功能是实现宫格式的循环式滚动，可以用来做魔方、对对碰、拼图、解密类游戏。此Demo只作了操作功能的实现，至于功能性的逻辑实现可以根据不同的产品需求和项目意图来添加设定。

    在Demo类的“GridView.m”里面有多处注释掉的代码，是为UIButton添加title的代码，打开后，显示的是该节点(UIButton)所处的行与列的索引值，方便用于别人研究实现逻辑用的。

    1.将GridView和HSCButton类对应的.h和.m及5张图片文件导入你的工程中。

    2.在你的试图中导入GridView的头文件，即#import “GridView.h”

    3.在你的试图初始化方法中，进行如下初始化

    /*

    参数：

     initWithFrame 设置动画区域的frame即：坐标和大小

     horizontCount 水平方向的个数

     verticalCount 垂直方向的个数

     提醒：设置参数的时候最好使用正矩形，倒不是因为好计算，是因为好看

     */

     GridView *gridView = [[GridView alloc] initWithFrame:CGRectMake(75, 100, 200, 200) 

     horizontCount:5 

     verticalCount:5];

     [self.view addSubview:gridView];

     [gridView release];

    如有需优化和不足的地方，望高手指正。
